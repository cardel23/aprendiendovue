import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import LastArticles from './components/LastArticles.vue'
import HelloWorld from './components/HelloWorld.vue'
import Vuelidate from 'vuelidate';
import Blog from './components/Blog.vue'
import Formulario from './components/Formulario.vue'
import Pagina from './components/Pagina.vue'
import Error404 from './components/Error404.vue'
import Peliculas from './components/Peliculas.vue'

Vue.config.productionTip = false

// Configuración del router para Vue
Vue.use(VueRouter);
Vue.use(Vuelidate); //activar validador
const routes = [
  {path: '/home', component: LastArticles},
  {path: '/blog', component: Blog},
  {path: '/formulario', component: Formulario},
  {path: '/pagina/:id?', name:'pagina', component: Pagina},//Como pasar parametros url
  {path: '/peliculas', component: Peliculas},
  {path: '/comp', component: HelloWorld},
  {path: '/', component: LastArticles},
  {path: '*', component: Error404}
];

const router = new VueRouter({
  routes,
  mode: 'history'
});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
